#!/usr/bin/env bash

set -euo pipefail

#GH_REPO="https://github.com/dat-linux/releases-birt"
TOOL_NAME="birt"
TOOL_TEST="birt"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

curl_opts=(-fSL#)

if [ -n "${GITHUB_API_TOKEN:-}" ]; then
  curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
fi

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    sort -t. -k 1,1n -k 2,2n -k 3,3n -k 4,4n | awk '{print $2}'
}

list_all_versions_full() {
  echo "4.12.0-202211301856-linux.gtk.x86_64.tar.gz
4.13.0-202303022006-linux.gtk.x86_64.tar.gz
4.14.0-202312020807-linux.gtk.x86_64.tar.gz
4.15.0-202403270652-linux.gtk.x86_64.tar.gz
4.16.0-202406141054-linux.gtk.x86_64.tar.gz
4.17.0-202409160710-linux.gtk.x86_64.tar.gz
4.18.0-202412050604-linux.gtk.x86_64.tar.gz"
}

list_all_versions() {
  list_all_versions_full | cut -d "-" -f1
}

download_release() {
  local version filename url
  version="$1"
  filename="$2"
  rm -f "$filename" &
  verfull="`list_all_versions_full | grep $version`"
  #url="https://download.eclipse.org/birt/downloads/drops/$verfull-linux.gtk.x86_64.zip"
  url="https://mirror.umd.edu/eclipse/birt/updates/release/$version/downloads/birt-report-designer-all-in-one-$verfull"
  echo "* Downloading $TOOL_NAME release $version..."
  echo "\"${curl_opts[@]}\" -o \"$filename\" -C - \"$url\""
  curl "${curl_opts[@]}" -o "$filename" -C - "$url" || fail "Could not download $url"
  printf "\\n"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    mkdir -p "$install_path/bin"
    cp -r "$CARI_DOWNLOAD_PATH"/* "$install_path"
    touch "$install_path/bin/birt"
    mkdir -p "$HOME/.birt/workspace"
    echo "#!/bin/bash" >> "$install_path/bin/birt"
    echo "if [[ -f $install_path/eclipse ]]; then 
      $install_path/eclipse -data $HOME/.birt/workspace
    else
      $install_path/birt -data $HOME/.birt/workspace
    fi" >> "$install_path/bin/birt"
    chmod a+x "$install_path/bin/birt"
    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
}
